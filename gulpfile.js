const gulp = require("gulp");
const gulpCucumber = require("gulp-cucumber");
const gulpFilter = require("gulp-filter");
const tslint = require("gulp-tslint");
const debug = require("gulp-debug");
const mongoData = require("gulp-mongodb-data");
const typescript = require("gulp-tsc");
/**
 * Runs tslint task
 *
 * @param {boolean} fromGitHook
 * @returns {Object} Gulp task
 */
function tslintTask(fromGitHook) {
    return gulp.src(["**/*.ts", "!node_modules/**", "!typings", "!**/*.d.ts"])
        .pipe(tslint({
        formatter: "verbose"
    }))
        .pipe(tslint.report());
}
/**
 * Runs cucumber task
 *
 * @param {Object} options
 * @returns {Object} gulp task
 */
function cucumberTask(options) {
    forceExit();
    return gulp.src("features/*")
        .pipe(gulpCucumber(options));
}
/**
 * Compilation des TypeScripts avant commit
 */
function tscTask() {
    return gulp.src(["**/*.ts", "!node_modules/**", "!typings", "!**/*.d.ts"])
        .pipe(typescript({ "target": "ES6", "module": "commonjs", "keepTree": false, "sourceMap": true }))
        .pipe(gulp.dest(""));
}
/**
 * Kills process on gulp stop
 */
function forceExit() {
    gulp.on("stop", function () {
        process.nextTick(function () {
            process.exit(0);
        });
    });
}
gulp.task("pre-commit", ["git-unit", "git-tslint"], () => { });
gulp.task("post-merge", () => { });
gulp.task("git-tsc", () => {
    tscTask();
});
gulp.task("git-tslint", () => {
    return tslintTask(true);
});
gulp.task("tslint", () => {
    return tslintTask(false);
});
gulp.task("unit", () => {
    return cucumberTask({
        "steps": "features/steps/*.js",
        "support": "*features/support/*.js",
        "format": "pretty"
    });
});
gulp.task("git-unit", ["git-tsc"], () => {
    return cucumberTask({
        "steps": "features/steps/*.js",
        "support": "*features/support/*.js",
        "format": "progress"
    });
});
/*
* requis: mongodb serveur roule.
*/
gulp.task("db-init", () => {
    return gulp.src("dev/db/*.json")
        .pipe(mongoData({
        mongoUrl: "mongodb://localhost/lieuxBatiments",
        dropCollection: true
    }));
});
//# sourceMappingURL=gulpfile.js.map