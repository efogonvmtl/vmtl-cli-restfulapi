# Restful API {{project}}

<!-- toc -->

<!-- tocstop -->

## Environnements
Cette section énumère les environnements de staging pour le projet {{project}}. 

### DEV
#### URL Swagger

### UAT
#### URL Swagger

### PROD
#### URL Swagger

## Environnement de Développement
### IDE
Les développeurs sont libres d'utiliser l'éditeur de leur choix parmi ceux énumérés ci-dessous.
Toutefois le dossier du projet ne doit pas contenir les fichiers propres à l'éditeur. Par exemple, le dossier .vscode doit
obligatoirement être exclus du référentiel GIT. 

#### [Visual Studio Code](https://vscode.microsoft.com)
Visual Studio Code (VSCode) est un environnement de développement élaboré par Microsoft qui supporte le développement et 
le déboguage d'applications NodeJS. Il dispose d'un modèle d'extensibilité qui ajoutent à ses fonctionnalités.

##### Extensions
Les exensions permettent d'ajouter aux fonctionnalités existantes à VSCode. Il est possible sous certaines conditions et configurations 
réseau que le panneau des extensions ne fonctionne pas directement dans VSCode. Si tel est le cas, veuillez utiliser les URL vers les VSIX. 
Pour installer une extension manuellement, simplement ouvrir le fichier VSIXPackage directement de VSCode:

- Renommer le ficher avec l'extension .VSIX
- Cliquer Fichier
- Cliquer Ouvrir le fichier...
- Ouvrir le fichier VSIX

Ceci est un résumé des [instructions de Microsoft](https://code.visualstudio.com/docs/editor/extension-gallery?pub=dbaeumer&ext=jshint)

Voici les extensions requises pour ce projet:

###### Obligatoires: 

1. [tslint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)

Intègre TSLint dans VS Code.

Télécharger le VSIX [ici](https://eg2.gallery.vsassets.io/_apis/public/gallery/publisher/eg2/extension/tslint/0.5.33/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage)

###### Optionnelles

1. [Cucumber (Gherkin) Syntax and Snippets](https://marketplace.visualstudio.com/items?itemName=stevejpurves.cucumber)

    Surligneur de syntaxe et snippet pour la nomenclature Gherkin

    Télécharger le VSIX [ici](https://stevejpurves.gallery.vsassets.io/_apis/public/gallery/publisher/stevejpurves/extension/cucumber/0.9.6/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage)

2. [Gulp Snippets](https://marketplace.visualstudio.com/items?itemName=tanato.vscode-gulp)

    Snippets pour Gulp.

    Télécharger le VSIX [ici](https://tanato.gallery.vsassets.io/_apis/public/gallery/publisher/tanato/extension/vscode-gulp/0.0.4/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage)

3. [Document This](https://marketplace.visualstudio.com/items?itemName=joelday.docthis)

    Snippets pour JSDoc.

    Télécharger le VSIX [ici](https://joelday.gallery.vsassets.io/_apis/public/gallery/publisher/joelday/extension/docthis/0.3.1/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage)

#### Sublime Text
#### Atom
#### Brackets

## Fondations Technologiques
Cette section décrit les fondations technologiques sélectionnées en fonction des objectifs non fonctionnels.

### Serveur RESTful NodeJS
Le serveur RESTful fourni les services consommés par diverses applications. Comme il n'est

Les services RESTful sont exposés par:

#### [Restify](http://restify.com/)
Restify est un module NodeJS développé spécifiquement pour élaborer des services REST corrects et performants. Restify emprunte beaucoup à ExpressJS tout 
en excluant le volet application Web.

### Intégrations Patrimoniales
L’intégration patrimoniale consiste en la mise en place du nouveau système/application à l’intérieur d’un ensemble de systèmes d’information existants.

### Intégrations Données
#### SQL
[KNEX](http://knexjs.org/)

Constructeur de requêtes SQL supportant les dialectes Postgres, MSSQL, MySQL, MariaDB, SQLite3 et Oracle.

#### NoSQL
[Mongoskin](https://GIThub.com/kissjs/node-mongoskin)

Un module d'abstraction du pilote MongoDB Natif pour NodeJS.

### Journalisation
[Bunyan](https://GIThub.com/trentm/node-bunyan)

Un module simple et rapide de journalisation JSON pour les services NodeJS.

    Étapes à utiliser:
        1. var logger = global.logger;
            Description: logger est déjà importé à l'object global, il est mieux de faire un raccourci.
        2. logger.infoReq(req);
            Description: journaliser l'object de request dans un controller (routeur);
        3. logger.infoRes(res);
            Description: journaliser l'object de response dans un controller (routeur);
        4. logger.info('info level log'); 
           logger.info('%s log', 'info level'); 
           logger.info('info', 'level', 'log');      
           logger.info({extraInfo: 'extra information'}, 'info level log');
            Description: 4 façon supportables pour journaliser, même pour warn, error, fatal.
        5. logger.warn('warning level log');
        6. logger.error('error level log');
            Description: ce type de log est affiché sur stdout et enregistré dans log/error.txt
        7. logger.fatal('fatal level log');   
            Description: ce type de log est affiché sur stdout et enregistré dans log/error.txt       

### Documentation
[Swagger-Restify](https://www.npmjs.com/package/swagger-restify) génère la documentation de l'API et respecte la version 2.0 de [Swagger](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#user-content-swagger-object). 

Le document JSON généré par le module est disponible à l'URL /api-docs.json 

ex: [https://localhost:7337/api/{{project}}/swagger](https://localhost:7337/api/{{project}}/swagger)  

#### Documenter l'API
Les routes dans Restify sont configurée par Swagger. L'API doit donc en tout premier lieu être documentée sous format YAML dans le fichier */api/swagger/swagger.yaml*. 
La documentation Swagger fait donc foi de fondation de l'API et Restify exprime sous forme de services cette documentation.
La documentation YAML applique le standard suivant: [https://github.com/OAI/OpenAPI-Specification/tree/master/schemas/v2.0](https://github.com/OAI/OpenAPI-Specification/tree/master/schemas/v2.0)

Consulter ce [lien](https://github.com/swagger-api/swagger-node) pour travailler avec Swagger et le développement de l'API de Lieux et Bâtiments

Les directives de documentation se trouvent ici: [Documentation des API](#documentation-des-api)

### Mise à Jour des Données
Durant le développement, il peut être nécessaire de disposer de données de chargement. Ces données doivent êtres chargées dans un fichier JSON se trouvant sous le dossier *dev/db*. 
Par exemple, les données de références se trouvent dans le fichier references.json. 

#### Tâche Gulp de Mise à Jour
Afin de mettre à jour la base de données, effectuer la commande suivante:

`$ gulp db-init`

### Sécurité
[Passport](http://passportjs.org/)

Authentification simplifiée pour NodeJS supportant diverses stratégies.

### Style de Code
[tslint](https://palantir.github.io/tslint/)
Cet outil est nécessaire pour valider statiquement la qualité du code. Il est nécessaire d'installer sur le poste les modules NodeJS à l'aide de cette commande:

    npm install -g tslint typescript

### Tests Unitaires
Les tests unitaires sont écrits en mode Behavior Driven Development selon la syntaxe Gherkin et sont exécutés en tant que tâche 
Gulp à l'aide du module [suivant](https://www.npmjs.com/package/gulp-cucumber).

### Automatisation des Tâches
Les tâches sont automatisées à l'aide de [Gulp](http://gulpjs.com/). Il faut installer Gulp globalement à l'aide de la commande suivante:

    npm install -g gulp

## Normes et Standards

### Branches
Le code doit être développé dans la branche *dev*. Les branches *test* et *prod* servent à la mise en route de l'application sur les serveurs dédiés à ces environnements.

Les développeurs sont invités à travailler sur des branches personnelles/features afin d'éviter la pollution de la branche de développement. 

### Code
Les standards sont renforcés par JSHint et JSCS à travers l'éditeur et les hook GIT. Les fichiers .jshintrc et .jscsrc contiennent les règles
et standards appliqués au projet.

>Si l'environnement de développement est Visual Studio Code, utiliser la combinaison *alt-shift-f* pour formatter le code source.
Ceci aura pour effet de re-formatter le code selon les normes et standards du projet automatiquement.

Une tâche gulp pre-commit a été appliquée afin de s'assurer de la validité du code avant tout commit à GIT.

### Tests Unitaires
Les normes et standards du projet édictent que les tests unitaires devraient être rédigés afin de valider les calculs. Il n'y a pas 
cibles de couverture. 

### Documentation du Code
Les standards de documentation JSDoc sont appliqués. Le [site](http://usejsdoc.org/) du projet décrit les balises de documentation existantes.

Une balise JSDoc se rédige de cette façon

```javascript
/**
* @function
* 
* méthode Hello World
* @param {String} nom Nom de la personne à qui l'on dit bonjour
* @returns {String} Chaîne contenant le message de bonjour
*/
```

La documentation est obligatoire.

#### Constructeurs
La balise de documentation de constructeur doit contenir: 

1. l'attribut  `@constructor`

#### Méthodes
La balise de documentation de méthode doit contenir:

1. l'attribut `@param {type} nom_de_la_variable explication` pour chacun des paramètre de la méthode.
2. l'attribut `@returns {type} explication` à noter que si aucune valeur n'est retournée, **{type}** doit être **undefined**.

## Documentation des API
Les APIs sont documentées au préalable et sont intégrées par Swagger à Restify. Cette section énumère les bonnes pratiques concernant la définition des APIs dans le fichier swagger.yaml.

### Entête de Fichier

```yaml
swagger: "2.0"
info:
  version: "0.0.1"
  title: Some API
# during dev, should point to your local machine
host: localhost:31337
# basePath prefixes all resource paths 
basePath: /api
# 
schemes:
  # tip: remove http to make production-grade
  - https
# format of bodies a client can send (Content-Type)
consumes:
  - application/json
# format of the responses to the client (Accepts)
produces:
  - application/json
```

#### BasePath (/basePath)
Doit toujours correspondre au format suivant:

    /api/{projet}

#### Protocols (/schemes)
Seul le protocol HTTPS est supporté

#### Consumes / Produces (/consumes et /produces)
Les données doivent être échangées dans le format 

    application/json

Il est possible de supporter plus d'un format d'échange toutefois.

### Chemins d'API (/paths)
Les chemins doivent référer à une ressource sur laquelle un ou plusieurs verbes HTTP peut s'appliquer. 

ex:
    Le service lieux (**/api/locations/v1/lieux**) supporte les verbes suivants: 
    
    - GET
    
    - POST
    
    - PUT
    
    - DELETE

Une définition complète d'un chemin ressemble à ceci:

```yaml
paths:
  /v1/store/{name}/{path}:
    # binds a127 app logic to a route
    x-swagger-router-controller: storage
    delete:
      description: Deletes file at path and returns stream upload url
      operationId: delete
      parameters:
        - name: name
          in: path
          description: store name
          required: true
          type: string
        - name: path
          in: path
          description: lists items at storage path
          required: true
          type: string
        - name: Authorization
          in: header
          description: authorization bearer token
          required: false
          type: string
        - name: X-AppId
          in: header
          description: application identification
          required: true
          type: string        
        - name: file
          in: body
          description: file description
          required: true
          schema:
            $ref: "#/definitions/File"
      responses:
        "200":
          description: Success
          schema:
            $ref: "#/definitions/DataUploadResponse"
        "401":
          description: Unauthorized
          schema:
            $ref: "#/definitions/ErrorResponse"
        "403":
          description: Forbidden
          schema:
            $ref: "#/definitions/ErrorResponse"
        "404":
          description: Not Found
          schema:
            $ref: "#/definitions/ErrorResponse"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
```

#### Chemins (/paths/{chemin})
Le chemin permet d'identifier la version de l'API et la ou les ressources à accéder pour chacun des verbes.

ex:

*/api/locations/v1/lieux*
 
Ce chemin permettra d'obtenir une liste (GET) des lieux, de créer (POST) ou mettre à jour (PUT) un ensemble de lieux ou de supprimer (DELETE) un ensemble de lieux

*/api/locations/v1/lieux/{id}*

Ce chemin permettra d'obtenir (GET) un lieu, de créer (POST) ou mettre à jour (PUT) un lieu ou de supprimer (DELETE) un lieu

#### x-swagger-router-controller (/paths/{chemin}/x-swagger-router-controller)
Ce paramètre définit quel contrôleur contient le code pour l'action à exécuter.

#### Les Verbes (/paths/{chemin}/{verbe})
Les verbes HTTP ont chacun une application particulière et doivent respecter l'intention sémantique du verbe:

##### GET
Retourne une ou des valeurs du type de la ressource résultant d'une requête.
##### POST
Crée une ou des valeurs du type de la ressource.
##### PUT
Met à jour une ou des valeurs du type de la ressource.
##### DELETE
Supprime une ou des valeurs du type de la ressource.

#### Identifiant de l'opération (/paths/{chemin}/{verbe}/operationId)
Ce paramètre lie le verbe à une méthode du contrôleur.

##### Paramètres (/paths/{chemin}/{verbe}/parameters)
Définir ici tous les paramètres requis pour l'exécution de la requête

Il existe divers types (/paths/{chemin}/{verbe}/parameters[i]/in) de paramètres et chacun a son utilité

- query
    
    Correspond aux paramètres sous forme (ex: /api/{application}/{version}/{ressource}**?val1=valeur1&val2=val2**).    
    Utiliser ces paramètres lorsqu'optionnels et lorsqu'ils décorent l'accès à une ressource comme par exemple dans le cas de la pagination ou du filtrage.
    
    S'applique aux verbes HTTP GET, DELETE et HEAD.

- path

    Correspond aux paramètres intrinsèques de l'accès à la ressource. Ces paramètres sont contenus dans le chemin d'accès à la ressource (ex: /api/{application}/{version}/{ressource}/**{identifiant}**). 
    Ces paramètres sont à utiliser lorsqu'obligatoires et sont des composants de la ressouces, les cas classiques étant: l'identifiant unique, sous catégorie, région, etc. 
    
    S'applique à tous les verbes HTTP.

- body

    Correspond aux données soumises à un service. Un paramètre de ce type est forcément obligatoire et une action ne peut en disposer
    plus qu'un. 
    
    Ne s'applique pas dans le cas des verbes HTTP GET, HEAD, et DELETE. 

- form

    Correspond aux données soumises par le biais d'un formulaire. Ces paramètres peuvent ou non être optionnels. Il peut y en avoir plus qu'un. 

    S'applique aux verbes PUT et POST.

- header

    Les paramètres échangés dans les HEADERS sont globaux à l'application et servent généralement à transporter les valeurs tels que le **Content-Type** ou **Authorization**.

    S'applique à tous les verbes.

#### Réponses (/paths/{chemin}/{verbe}/responses)

Les réponses doivent être définies par code de retour HTTP selon les standards. La liste des codes de retour se trouve ici: [https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP)

La structure de retour en cas d'erreur doit toujours être la même (**schema/$ref: "#/definitions/ErrorResponse"**)

Les tests automatisés devraient normalement vérifier que chacun des codes de retour est bel et bien retourné le cas échéant.

### Schémas (/schemas)

Les définitions de schémas sont essentiels afin que Swagger puisse prévalider les données reçues. Il est possible ainsi d'éviter de valider manuellement les valeurs obligatoires et le format de celles-ci le cas échéant.

Voici un exemple de schéma:

```yaml
# complex objects have schema definitions
definitions:
  DataUploadResponse:
    description: File upload stream descriptor
    properties:
      link:
        type: string
  FileListResponse:
    description: Files list object.
    properties:
      files:
        type: array
        items:
          $ref: "#/definitions/File"
  File:
    description: File descriptor
    properties:
      name:
        type: string
      tag:
        type: string
      modifiedDate:
        type: number
      type:
        type: string
        enum:
          - file
          - folder
      mime:
        type: string
      link:
        type: string
    required:
      - name
      - tag
      - mime
      - type
  ErrorResponse:
    required:
      - message
    properties:
      message:
        type: string
```

#### Validation

Swagger permet une validation minimale des paramètres en entrée/sortie via la structure des messages. 
L'article suivant [https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#parameter-object](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#parameter-object) indique les capacités de validation en entrée possibles.

## Architecture de la Solution 
Cette section présente l'architecture de l'application à haut niveau. 

L'application Restify Lieux et Bâtiments repose sur une architecture de type: 

***Contrôleurs - Services - Repositories.***

Un découpage de telle manière fait en sorte que chacune des couches est indépendantes et peut être testée unitairement.

### Contrôleurs

Les contrôleurs sont le point d'entrée de l'API et doivent correspondre à la définition selon le fichier swagger.yaml.

Les normes pour les contrôleurs sont celles-ci:

1. Le nom du fichier doit correspondre à l'attribut **x-swagger-router-controller**
2. Les méthodes (ou actions) doivent porter le nom du verbe auquel ils répondent
> Ex: `function get(req, res, next){}` pour le verbe **GET**, `function post(req, res, next){}` pour le verbe **POST**, etc. 
Il est possible de définir l'operationId au niveau du path mais il faut s'assurer de l'unicité au global auquel cas Swagger déterminera la définition d'API comme étant invalide.
3. Les paramètres d'entrées sont *req, res et next* et il est essentiel de terminer un appel réussi avec `return next();`
Exemple:
``` javascript 
function post(req, res, next) {
        res.send(200, {});
        return next();
}
```
4. Les erreurs doivent être générées par [restify-errors](https://github.com/restify/errors). Le module produit un message de retour conforme à la définition d'API Swagger (ErrorResponse).

``` javascript
/**
 * Liste les lieux en fonction des paramètres de requête
 *
 * @param {any} req objet définition de requête
 * @param {any} res objet définition de réponse
 * @param {any} next appel suivant
 */
function get(req, res, next) {
    ControllerBase.authenticate(req, res, function () {
        var authError = new errors.InvalidVersionError({
             statusCode: 401,
             message: 'Unauthorized'
        });
        res.send(authError);
        return next();
    });
}
```

5. Les niveaux de logging sont les suivants (trace, info, warn, error et fatal) et il faut les utiliser de manière appropriées. Le logger est configuré de telle façon à rediriger les logs selon le niveau au(x) bon(s) destinataire(s).
    1. Quand logger une trace?

        Lorsque l'information n'est utile que pour le déboguage. Produit généralement beaucoup de bruit et une trace est rarement conservée de manière permanente.

    2. Quand logger une info?

        Lorsque l'information est utile à des fin d'audit (voir questions: Qui? Quand? Où? Comment?).
        
    2. Quand logger un avertissement (warn)?

        Lorsque l'information est utile à des fin d'audit (voir questions: Qui? Quand? Où? Comment?) et que le résultat produit un résultat contraire à une règle d'affaire non terminale.
        
    2. Quand logger une erreur (error)?

        Lorsque l'information est utile à des fin d'audit (voir questions: Qui? Quand? Où? Comment?) et que le résultat produit un résultat contraire à une règle d'affaire terminale.

    2. Quand logger une erreur fatale (fatal)?

        Lorsque l'erreur place le système dans un état d'instabilité et qu'il doit être remis sur pied.

    2. Comment logger un(e) trace, info, warn, error et fatal: 
    ```javascript    
    req.log.trace('Quelque chose à trace');
    req.log.info('Quelque chose à info');
    req.log.warn('Quelque chose à warn');
    req.log.error('Quelque chose à error');
    req.log.fatal('Quelque chose à fatal');
    ```
    Il incombe au développeur de logger les événements appropriés. Pour Restify par exemple, une erreur 403 retournée consiste en une entrée au niveau **trace**.
    
    Il incombe au développeur de gérer les exceptions dans son action de contôleur.
    Par exemple une erreur non gérée sera gérée par Swagger et retournée sous forme d'erreur 500.

    ```HTTP
    HTTP/1.1  
 
    Content-Type: application/json
    
    {"message":"HAHAHAHA PWNED"}
    ```
6. Seuls les contrôleurs sont habilités à déterminer le code d'erreur à retourner au client. 

### Services

Les services sont utilisés par les contrôleurs pour atteindre les objectifs d'affaire. Aucun code métier ne devrait se retrouver dans un contrôleur.
Les services ne devraient pas avoir accès aux objets **Request** et **Response** puisqu'il s'agit d'éléments propres au actions de contrôleurs.

### Repositories

Les repositories sont utilsés par les services en tant que couche d'accès aux données. Les services doivent être isolés de l'accès aux données.

### Intégration Contrôleurs/Services/Repositories

Chaque module service, contrôleur est responsable de charger ses dépendances par le biais de la méthode *require*.
Pour le moment, une stratégie d'injection de dépendance n'est pas envisagée.

#### Méthodes Asynchrones

Les appels entre les diverses couches doivent utiliser le modèle par "callback", c-à-d:

```javascript
var handleAuthentication = (err, result) => {
    // Do something
}
// Authentifier l'usager et déléguer au callback
authenticationService.authenticate(req.params.username, req.params.password, handleAuthentication);
```

Le format doit se conformer à celui de Restify ex: function(error, result)

## Mise en Route
Les étapes suivantes décrivent comment metre en place le projet pour développement. 

1. Configurer les accès
    1. Après avoir reçu une invitation pour se joindre à BitBucket, créer un compte.
    2. Consulter [https://bitbucket.org/villemontreal/{{project}}-api](https://bitbucket.org/villemontreal/{{project}}-api)

2. Cloner le projet
    1. Démarrer GIT Bash
    2. Se déplacer dans le répertoire où le projet sera cloné (ex: `$ cd /c/w`)
    3. `$ git clone https://bitbucket.org/villemontreal/{{project}}.git`
    4. Le mot de passe et le nom d'utilisateur sont ceux entrés à l'étape 1.1.

3. Installer les pré-requis
    1. `$ cd /c/w/{{project}}`
    2. `$ npm install`
    3. `$ npm install -g gulp tslint typescript cucumber`

4. Démarrer l'application
    1. `$ npm start`