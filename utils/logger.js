"use strict";
/*
* Nom du fichier: logger.js
* Description: Définir la fonctionnalité de journalisation
* Copyright (c) Ville de Montréal
*/
/// <reference path="../typings/bundle.d.ts"/>
const bunyan = require("bunyan");
const fs = require("fs");
const path = require("path");
const restifyErrors = require("restify-errors");
/*
* Description to attributes:
*   name (required): logger name
*   src: add file name and line number to log if in development environment.
*   streams: define the output of each log level, included file, stdout or slash etc.
*   serializers: specialize to req, res and error object, define a filter for log information
*/
class Logger {
    constructor() {
        if (!Logger.logger) {
            Logger.logger = bunyan.createLogger({
                name: "lieux-et-batiments",
                src: process.env.NODE_ENV === "development",
                streams: [
                    {
                        level: "trace",
                        stream: process.stdout
                    },
                    {
                        level: "info",
                        path: path.join(process.cwd(), "log/error.txt"),
                        type: "rotating-file",
                        period: "1d",
                        count: 50
                    },
                ],
                serializers: {
                    req: this.reqSerializer,
                    res: this.resSerializer,
                    err: restifyErrors.bunyanSerializer
                }
            });
            try {
                var logFile = path.join(__dirname, "../log/error.txt");
                fs.closeSync(fs.openSync(logFile, "w"));
            }
            catch (e) {
                console.log(e);
            }
        }
    }
    get logger() {
        return Logger.logger;
    }
    /*
     * request object filter (to be continued)
     */
    reqSerializer(req) {
        return {
            username: req.username,
            method: req.method,
            url: req.url,
            remoteAddress: req.remoteAddress
        };
    }
    /*
     * response object filter (to be continued)
     */
    resSerializer(res) {
        return {
            statusCode: res.statusCode
        };
    }
    /*
     * error object filter (to be continued)
     */
    errSerializer(err) {
        return {
            message: err.message
        };
    }
}
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map