FROM node:6.3

#Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Install all dependencies
COPY package.json /usr/src/app
RUN npm install

#Bundle app source
COPY . /usr/src/app

EXPOSE 7337
CMD ["npm", "start"]