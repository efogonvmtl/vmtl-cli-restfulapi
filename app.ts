import * as restify from "restify";
import * as fs from "fs";
import {Logger} from "./utils/logger";

const swaggerRestify = require("swagger-restify-mw"),
    passport = require("passport-restify"),
    passportSetup = require("./bootstrap/passport-local-setup");

class Server {
    private static server;

    public static init() {
        if (!Server.server) {
            // configs
            let port = process.env.PORT || 7337;
            let sslKey = fs.readFileSync("./certs/server.key").toString("utf8");
            let sslCert = fs.readFileSync("./certs/server.crt").toString("utf8");
            let options = {
                appRoot: __dirname,
                key: sslKey,
                certificate: sslCert,
                name: "Lieux et Bâtiments API",
                log: new Logger().logger
            };

            Server.server = restify.createServer(options);

            Server.server.use(this.crossOrigin);
            Server.server.use(restify.bodyParser());
            Server.server.use(restify.jsonp());
            Server.server.use(passport.initialize());
            Server.server.use(restify.requestLogger());

            Server.server.on("uncaughtException", function (req, res, route, error) {
                req.log.fatal(error);
            });

            swaggerRestify.create(options, function (err, swaggerRestify) {
                if (err) { throw err; }
                swaggerRestify.register(Server.server);
                Server.server.listen(port, function () {
                    console.log("%s listening at %s", Server.server.name, Server.server.url);
                });
            });
        }
    }

    /**
     * Configure le serveur pour supporter les appels d'origine externe
     *
     * @param {any} req objet de requête
     * @param {any} res objet de réponse
     * @param {any} next méthode suivante dans le pipeline
     * @returns {undefined}
     */
    private static crossOrigin(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        return next();
    }
}

Server.init();





