"use strict";
const passport = require("passport-restify");
/**
 * Classe de base des contrôleurs Restify
 *
 * @export
 * @class ControllerBase
 */
class ControllerBase {
    /**
     * Creates an instance of ControllerBase.
     *
     */
    constructor() {
        this.version = 1;
        let passport = require("passport-restify");
        this.passport = passport;
    }
    /**
     * Authentifie le requérant en fonction de l'objet de requête
     *
     * @param {any} req
     * @param {any} res
     * @param {any} next
     */
    authenticate(req, res, next) {
        passport.authenticate("custom", { session: false })(req, res, next);
    }
    ;
    /**
     * Retourne le préfixe de l'url associé à l'action
     *
     * @returns
     */
    getUrlPrefix() {
        return "/api/" + this.application + "/v" + this.version + "/" + this.name + "/";
    }
    ;
}
exports.ControllerBase = ControllerBase;
//# sourceMappingURL=controller-base.js.map