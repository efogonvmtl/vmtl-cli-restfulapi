"use strict";
const controller_base_1 = require("./controller-base");
const hello_service_1 = require("./../../services/hello-service");
const errors = require("restify-errors");
const logger = require("./../../utils/logger");
/**
 * Contrôleur Hello
 *
 * @export
 * @class HelloController
 * @extends {ControllerBase}
 */
class HelloController extends controller_base_1.ControllerBase {
    constructor() {
        super();
        /**
         * Exécute l'action GET
         *
         * @param {any} req
         * @param {any} res
         * @param {any} next
         */
        this.get = (req, res, next) => {
            const findCallback = (err, result) => {
                if (err) {
                    res.send(400, err);
                }
                else {
                    let retVal = { "data": [] };
                    result.forEach((element) => {
                        retVal.data.push(element);
                    }, this);
                    res.send(200, retVal);
                }
                return next();
            };
            const action = () => {
                let nom = req.swagger.params.nom && req.swagger.params.nom.value;
                this.service.find(nom, findCallback);
            };
            super.authenticate(req, res, action);
        };
        super.name = "hello";
        this.service = new hello_service_1.HelloService();
    }
}
exports.HelloController = HelloController;
const controller = new HelloController();
module.exports = {
    "get": controller.get
};
//# sourceMappingURL=hello-controller-v1.js.map