import {ControllerBase} from "./controller-base";
import {HelloService} from "./../../services/hello-service";

const errors = require("restify-errors");
const logger = require("./../../utils/logger");

/**
 * Contrôleur Hello
 * 
 * @export
 * @class HelloController
 * @extends {ControllerBase}
 */
export class HelloController extends ControllerBase {
    util: any;
    errors: any;
    service: any;
    logger: any;
    constructor() {
        super();
        super.name = "hello";
        this.service = new HelloService();
    }
    /**
     * Exécute l'action GET
     * 
     * @param {any} req
     * @param {any} res
     * @param {any} next
     */
    get = (req, res, next) => {
        const findCallback = (err, result) => {
            if (err) {
                res.send(400, err);
            }
            else {
                let retVal = { "data": [] };
                result.forEach((element) => {
                    retVal.data.push(element);
                }, this);
                res.send(200, retVal);
            }
            return next();
        };
        const action = () => {
            let nom = req.swagger.params.nom && req.swagger.params.nom.value;
            this.service.find(nom, findCallback);
        };
        super.authenticate(req, res, action);
    };
}
const controller = new HelloController();
module.exports = {
    "get": controller.get
};