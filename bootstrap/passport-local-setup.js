const passport = require('passport-restify'),
    CustomStrategy = require('passport-custom');

passport.use(new CustomStrategy(
    authenticate
));

passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});

/**
 * Authentification d'un utlisateur sur la base d'un nom et mot de passe
 *
 * @param {any} req objet de requête
 * @param {any} done callback de complétion
 * @returns {undefined}
 */
function authenticate(req, done) {
    req.user = {};
    return done(null, req.user);
}