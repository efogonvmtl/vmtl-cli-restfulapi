import {UserRepository} from "./data-access/users-repository";
/**
 * Service d'affaire Hello
 * 
 * @export
 * @class HelloService
 */
export class HelloService {
    repository: any;
    constructor() {
        this.repository = new UserRepository();
    }

    /**
     * Retourne les données hello par nom
     * 
     * @param {any} nom
     * @param {any} callback
     */
    find(nom, callback) {
        this.repository.find({ "nom": nom }, callback);
    }
}

