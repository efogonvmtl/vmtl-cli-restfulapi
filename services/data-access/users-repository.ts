import {MongoDatabase}  from "./mongo-database";

/**
 * Dépôt de données des lieux 
 * 
 * @class UserRepository
 */
export class UserRepository {
    mongo: MongoDatabase;
    users: any;
    /**
     * Creates an instance of HelloRepository.
     * 
     */
    constructor() {
        this.mongo = new MongoDatabase();
        this.users = this.mongo.database.collection("user");
    }
    /**
     * Cherche à partir de l'objet de requête le hello correspondant 
     * 
     * @param {any} query
     * @param {any} callback
     */
    find(query, callback) {
        this.users.find(query).toArray(callback);
    }
}