const mongoskin = require("mongoskin");
const dbUrl = process.env["DBURL"] || "mongodb://localhost:27017/lieuxBatiments";

/**
 * MongoDatabase wrapping class
 * 
 * @export
 * @class MongoDatabase
 */
export class MongoDatabase {
    /**
     * Creates an instance of MongoDatabase.
     * 
     */
    constructor() {

    }
    /**
     * Gets underlying database instance
     * 
     * @readonly
     */
    get database() {
        return mongoskin.db(dbUrl);
    }
    /**
     * Converts a value into a mongo db recognized id
     * 
     * @param {any} value
     * @returns
     */
    toObjectId(value) {
        return mongoskin.helper.toObjectID(value);
    }
}