"use strict";
const mongo_database_1 = require("./mongo-database");
/**
 * Dépôt de données des lieux
 *
 * @class UserRepository
 */
class UserRepository {
    /**
     * Creates an instance of HelloRepository.
     *
     */
    constructor() {
        this.mongo = new mongo_database_1.MongoDatabase();
        this.users = this.mongo.database.collection("user");
    }
    /**
     * Cherche à partir de l'objet de requête le hello correspondant
     *
     * @param {any} query
     * @param {any} callback
     */
    find(query, callback) {
        this.users.find(query).toArray(callback);
    }
}
exports.UserRepository = UserRepository;
//# sourceMappingURL=users-repository.js.map