"use strict";
const users_repository_1 = require("./data-access/users-repository");
/**
 * Service d'affaire Hello
 *
 * @export
 * @class HelloService
 */
class HelloService {
    constructor() {
        this.repository = new users_repository_1.UserRepository();
    }
    /**
     * Retourne les données hello par nom
     *
     * @param {any} nom
     * @param {any} callback
     */
    find(nom, callback) {
        this.repository.find({ "nom": nom }, callback);
    }
}
exports.HelloService = HelloService;
//# sourceMappingURL=hello-service.js.map