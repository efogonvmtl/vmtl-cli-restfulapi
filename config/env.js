/*
* Nom du fichier: env.js
* Description: Définir des configurations utilisées par serveur
* Copyright (c) Ville de Montréal
*/

var env = {
    dbConUrl: '',
    port: 3000
};

module.exports = env;